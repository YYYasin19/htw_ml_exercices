from math import sqrt

import pandas as pd
from pandas import DataFrame
from sklearn.metrics import mean_squared_error
import numpy as np

# read the dataset
from sklearn import linear_model
from sklearn.model_selection import KFold, train_test_split

fifa_data = pd.read_csv('input_data/fifa_clean.csv')

# extract the target feature
fifa_target = fifa_data.iloc[:, [50]].values

# extract the features -- age , overall, potential, international reputation, height, weight
features: DataFrame = fifa_data.iloc[:, [4, 6, 7, 10, 14, 15]]

# split the data -- this is additional and not needed in kfold test validation
x_train, x_test, y_train, y_test = train_test_split(features, fifa_target, test_size=0.2)

# create a cross-validator (5) with random shuffle of the data
kf = KFold(n_splits=5, shuffle=True)

# create a regressor
regression = linear_model.LinearRegression()
rmse_values = []

# run through the splitted data
for train_index, test_index in kf.split(x_train):
    kx_train = x_train.iloc[train_index, :]
    kx_test = x_train.iloc[test_index, :]

    ky_train = y_train[train_index]
    ky_test = y_train[test_index]

    # train the model with this data
    regression.fit(kx_train, ky_train)

    # get the predictions for this round
    predictions = regression.predict(kx_test)

    # calculate error for this round
    rmse = sqrt(mean_squared_error(ky_test, predictions))
    rmse_values.append(rmse)
    pass


def number_format(number, decimal_places=2) -> str:
    return '{0:,}'.format(round(number, decimal_places)).replace('.', '~').replace(',', '.').replace('~', ',')
    pass


# this is not the best solution for this. but it works.
print(f'RMSE Values: {[number_format(i) + "€" for i in rmse_values]}\n'
      f'Mean: {number_format(np.mean(rmse_values))}€')
