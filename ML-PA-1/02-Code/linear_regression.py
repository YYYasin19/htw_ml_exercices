import pandas as pd
from pandas import DataFrame
import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model

# read dataset into frame
happiness_data = pd.read_csv('input_data/happiness-report-2016.csv')

# extract target feature
happiness_score: DataFrame = happiness_data.iloc[:, [3]].values

# get different features for scatter plot
economy: DataFrame = happiness_data.iloc[:, [6]].values
family: DataFrame = happiness_data.iloc[:, [7]].values
health: DataFrame = happiness_data.iloc[:, [8]].values
freedom: DataFrame = happiness_data.iloc[:, [9]].values
trust: DataFrame = happiness_data.iloc[:, [10]].values

# pack them into a list
features_list = [(economy, "economy"), (family, "family"), (health, "health"), (freedom, "freedom"), (trust, "trust")]

# scatter everyone with happiness_score
for feature in features_list:
    # set variables
    feature_data: DataFrame = feature[0]
    feature_name = feature[1]

    # calculate linear regression
    regression = linear_model.LinearRegression()
    regression.fit(feature_data, happiness_score)
    predictions: DataFrame = regression.predict(feature_data)

    # calculate parameters
    optimal_coefficient = regression.coef_
    # corr_coefficient = np.corrcoef(feature_data.reshape(1, -1), happiness_score.reshape(1, -1))
    model_corr = np.corrcoef(predictions.reshape(1, -1), happiness_score.reshape(1, -1))
    mse = sum((regression.predict(feature_data) - happiness_score) ** 2) / len(happiness_score)

    # plot this feature and the happiness score
    plt.scatter(feature_data, happiness_score, marker=".")

    plt.plot(feature_data, predictions.reshape(-1, 1), color='red', lw=2)

    # set labels and show plot
    plt.title(  # f'Data Correlation: {round(corr_coefficient[0, 1], 3)}\n'
        f'Model Correlation: {round(model_corr[0, 1], 3)}\n'
        f'MSE {round(mse[0],3)}\n'
        f'w_1 = {round(optimal_coefficient[0, 0],3)}, w_0 = {round(regression.intercept_[0],3)}')
    plt.ylabel('Happiness Score')
    plt.xlabel(f'{feature_name}'.upper())
    plt.show()
    pass
