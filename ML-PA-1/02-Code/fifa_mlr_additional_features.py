from math import sqrt

import numpy as np
import pandas as pd
from pandas import DataFrame
# read the dataset
from sklearn import linear_model
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import KFold

fifa_data = pd.read_csv('input_data/fifa_clean_additional_feature.csv', sep=";")

# extract the target feature
fifa_target = fifa_data.iloc[:, [50]].values

# extract the features
features: DataFrame = fifa_data.iloc[:, [4, 6, 7, 10, 14, 15]]

# # 'GKHandling', 'GKKicking', 'GKPositioning', 'GKDiving', 'GKReflexes'
# # 'Positioning', 'Vision', 'Agility', 'Dribbling', 'Finishing'

# get additional features
nationality = fifa_data.iloc[:, [5]]
club = fifa_data.iloc[:, [8]]
reaction = fifa_data.iloc[:, [29]]
special_value = fifa_data.iloc[:, [52]]
overall_potential = fifa_data.iloc[:, [6, 7]]
fifa_data['overall_potential'] = round(overall_potential.mean(axis=1), 0)
overall_potential = fifa_data.loc[:, 'overall_potential']

# one hot encode some of them
nationality_ohe = pd.get_dummies(nationality)
club_ohe = pd.get_dummies(club)

# add to features
# features = pd.concat([features, reaction, special_value, club_ohe, nationality_ohe, overall_potential], axis=1)
features = pd.concat([features, reaction, special_value, club_ohe, nationality_ohe, overall_potential], axis=1)
# create a cross-validator (5) with random shuffle of the data
kf = KFold(n_splits=5, shuffle=True)

# create a regressor
regression = linear_model.LinearRegression()
rmse_values = []

# run through the splitted data
for train_index, test_index in kf.split(features):
    kx_train = features.iloc[train_index, :]
    kx_test = features.iloc[test_index, :]

    ky_train = fifa_target[train_index]
    ky_test = fifa_target[test_index]

    # train the model with this data
    regression.fit(kx_train, ky_train)

    # get the predictions for this round
    predictions = regression.predict(X=kx_test)

    # calculate error for this round
    rmse = sqrt(mean_squared_error(ky_test, predictions))
    # print(f'Train RMSE: {sqrt(mean_squared_error(ky_train, regression.predict(kx_train)))}')
    rmse_values.append(rmse)
    pass


def number_format(number, decimal_places=2) -> str:
    return '{0:,}'.format(round(number, decimal_places)).replace('.', '~').replace(',', '.').replace('~', ',')
    pass


# this is not the best solution for this. but it works.
print(f'RMSE Values: {[number_format(i) + "€" for i in rmse_values]}\n'
      f'Mean: {number_format(np.mean(rmse_values))}€')
