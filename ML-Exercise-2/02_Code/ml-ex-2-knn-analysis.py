import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import MinMaxScaler
from scipy.spatial import distance
import math

data = pd.read_csv('ml-ex-2-knn-table.csv', sep=',')

x = data[['distance_to_beach', 'price_per_night', 'average_customer_rating']]

scaler = MinMaxScaler()
scaler.fit(x)
x_norm = scaler.transform(x)
x_norm = pd.DataFrame(x_norm)
first = [2.5, 500, 1]
fn = lambda xrow: math.sqrt((first[0] - xrow[0]) ** 2 + (first[1] - xrow[1]) ** 2 + (first[2] - xrow[2]) ** 2)

second = (0.5, 100, 4)
fn2 = lambda xrow: math.sqrt((second[0] - xrow[0]) ** 2 + (second[1] - xrow[1]) ** 2 + (second[2] - xrow[2]) ** 2)

col = x_norm.apply(fn, axis=1)
x_norm = x_norm.assign(c=col.values)

col2 = x_norm.apply(fn2, axis=1)
x_norm = x_norm.assign(c2=col2.values)

print(x_norm.to_latex(float_format='%.2f'))
